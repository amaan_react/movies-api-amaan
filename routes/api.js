const express = require('express')
const router = express.Router()

const config = require(`./config.js`)

const mysql = require('mysql');
var connection = mysql.createConnection({
  host     : config.HOST,
  user     : config.USERS,
  password : config.PASSWORD,
  database : config.DATABASE
});

connection.connect();

//Get all directors
router.get(`/directors`,(req, res,next) => {
    connection.query(`SELECT * FROM movies.Directors;`, function (error, results, fields) {
    if (error) next(error)
    res.send(results)
    })
});


//Get the director with given ID
router.get(`/directors/:id`,(req, res, next) => {
    connection.query(`SELECT * FROM movies.Directors WHERE DirectorId = ?;`,[req.params.id], function (error, results, fields) {
    if (error) next(error)
    res.send(results)
    })
});


//Add a new director
router.post('/directors', function (req, res) {
    var postData  = req.body;
    connection.query(`INSERT INTO movies.Directors SET ?`, postData, function (error, results, fields) {
       if (error) next(error)
       res.end(JSON.stringify(results));
    });
});

//Update the director with given ID
router.put('/directors/:id', function (req, res) {
    var postData  = req.body;
    connection.query(`UPDATE movies.Directors SET Director=? WHERE DirectorId=?`, [postData.Director, req.params.id], function (error, results, fields) {
       if (error) next(error)
       res.end(JSON.stringify(results));
    });
});


//Delete the director with given ID
router.delete(`/directors/:id`,(req, res) => {
    connection.query(`DELETE FROM movies.Directors WHERE DirectorId = ?;`,[req.params.id], function (error, results, fields) {
    if (error) next(error)
    res.send(`Successfully Deleted`)
    })
});


//Get all movies
router.get(`/movies`,(req, res,next) => {
    connection.query(`SELECT * FROM movies.Movies INNER JOIN movies.Directors ON Ranks=DirectorId;`, function (error, results, fields) {
    if (error) next(error)
    res.send(results)
    })
});


//Get the movies with given ID
router.get(`/movies/:id`,(req, res, next) => {
    connection.query(`SELECT * FROM movies.Movies WHERE Ranks = ?;`,[req.params.id], function (error, results, fields) {
    if (error) next(error)
    res.send(results)
    })
});


//Add a new Movie
router.post('/movies', function (req, res) {
    var postData  = req.body;
    connection.query(`INSERT INTO movies.Movies SET ?`, postData, function (error, results, fields) {
       if (error) next(error)
       res.end(JSON.stringify(results));
    });
});


//Update the movie with given ID
router.put('/movies/:id', function (req, res) {
    var postData  = req.body;
    connection.query(`UPDATE movies.Movies SET Title=?,Description=?,Runtime=?,Genre=?,Rating=?,Metascore=?,Votes=?,Gross_Earning_in_Mil=?,Actor=?,Year=? WHERE Ranks=?`, [postData.Director,postData.Title,postData.Description,postData.Runtime,postData.Genre,postData.Rating,postData.Metascore,postData.Votes,postData.Gross_Earning_in_Mil,postData.Actor,postData.Year, req.params.id], function (error, results, fields) {
       if (error) next(error)
       res.end(JSON.stringify(results));
    });
});


//Delete the movie with given ID
router.delete(`/movie/:id`,(req, res) => {
    connection.query(`DELETE FROM movies.Movies WHERE Ranks = ?;`,[req.params.id], function (error, results, fields) {
    if (error) next(error)
    res.send(`Successfully Deleted`)
    })
});


module.exports = router