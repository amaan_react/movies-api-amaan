const dotenv = require("dotenv")

configResult = dotenv.config()

if(configResult.error!= undefined){
    console.log(configResult)
}


HOST =process.env.HOST
USERS =process.env.USERS
PASSWORD =process.env.PASSWORD
DATABASE =process.env.DATABASE


module.exports = {
    HOST, USERS, PASSWORD, DATABASE        
}
