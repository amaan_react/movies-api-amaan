const express = require('express')
const app = express()
const port = 4488

const usersRouter = require('./routes/api.js');

const bodyparser = require(`body-parser`)
app.use(bodyparser.json())

app.use("/api", usersRouter)

app.use((request,response,next)=>{
    response.status(404).json({
        message: "404 page not found"
    }).end()
})    

app.use((error,request,response,next)=>{
    console.log(error.stack)
    response.status(500).json({
        message: "Error occurred due to an internal server error. Please try again later."
    }).end()
})

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
}) 