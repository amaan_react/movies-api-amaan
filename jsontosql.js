const mysql = require('mysql');
const movies = require('./movies.json');

var connection = mysql.createConnection({
  host     : `localhost`,
  user     : `root`,
  password : `Amaan@1998`,
  database : `movies`
});


connection.connect();

// Creating Directors Table

connection.query('CREATE TABLE IF NOT EXISTS Directors( DirectorId INTEGER  NOT NULL PRIMARY KEY  AUTO_INCREMENT,Director VARCHAR(20) NOT NULL);', function (error, results, fields) {
    if (error) throw error;
    console.log('The solution is: ', results);
});

for(let index=0; index<movies.length; index++) {
    connection.query(`INSERT INTO Directors(DirectorId,Director) VALUES(${movies[index].Rank},'${movies[index].Director}');`), function (error, results, fields) {
        if (error) throw error;
    };
}


//Creating Movies Table

connection.query('CREATE TABLE IF NOT EXISTS Movies( Ranks INTEGER  NOT NULL PRIMARY KEY  AUTO_INCREMENT,Title VARCHAR(49) NOT NULL,Description VARCHAR(246) NOT NULL,Runtime INTEGER  NOT NULL,Genre VARCHAR(9) NOT NULL,Rating NUMERIC(3,1) NOT NULL,Metascore VARCHAR(3) NOT NULL,Votes INTEGER  NOT NULL,Gross_Earning_in_Mil VARCHAR(6) NOT NULL,Actor VARCHAR(21) NOT NULL,Year INTEGER  NOT NULL);', function (error, results, fields) {
    if (error) throw error;
    console.log('The solution is: ', results);
});



for(let index=0; index<movies.length; index++) {
    connection.query(`INSERT INTO Movies(Ranks,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Actor,Year) VALUES(${movies[index].Rank}, "${movies[index].Title}", "${movies[index].Description}", ${movies[index].Runtime}, '${movies[index].Genre}', ${movies[index].Rating}, '${movies[index].Metascore}' ,${movies[index].Votes}, '${movies[index].Gross_Earning_in_Mil}' , '${movies[index].Actor}', ${movies[index].Year} );`), function (error, results, fields) {
        if (error) throw error;
    };
}

connection.end();